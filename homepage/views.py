from django.shortcuts import render
from django.shortcuts import redirect
from .models import Status
from . import forms
from .forms import StatusForm

# Create your views here.
def index(request):
	stats = Status.objects.all().values()
	if (request.method == 'POST'):
		form = StatusForm(request.POST)
		if (form.is_valid()):
			form.save()
			return redirect('/')
	else:
		form = StatusForm()
	return render(request, 'index.html', {'stats': stats, 'form':form})

def confirmation(request):
	if request.method == 'POST':
		statusForm = StatusForm(request.POST)  
		if statusForm.is_valid():
			name = statusForm.data['name']
			status = statusForm.data['status']
			confirmForm = StatusForm(initial={'name': name, 'status': status})
			return render(request, 'confirmation.html', {'confirmForm': confirmForm, 'confirmName': name, 'confirmStatus': status}) 
		else:
			response = redirect('/')
			return response
	if request.method == "GET":
		return redirect('/')
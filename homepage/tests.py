from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from .models import Status
from django.apps import apps
from homepage.apps import HomepageConfig
from .forms import StatusForm
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.
class TestUnit(TestCase):
	
	def test_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code,200)
		
	def test_create_status(self):
		new_status = Status.objects.create(name = 'name',status = 'test')
		self.assertTrue(isinstance(new_status, Status))
		self.assertTrue(new_status.__str__(), new_status.name)
		available_status = Status.objects.all().count()
		self.assertEqual(available_status,1)
	
	def test_template_used(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'index.html')
		
	def test_show_status(self):
		status = "test"
		data = {'message' : status}
		post_data = Client().post('/', data)
		self.assertEqual(post_data.status_code, 200)
		
	def test_app(self):
		self.assertEqual(HomepageConfig.name, 'homepage')
		self.assertEqual(apps.get_app_config('homepage').name, 'homepage')

	def test_greeting_exists(self):
		response = Client().get('/')
		response_content = response.content.decode('utf-8')
		self.assertIn("Hello, how are you?", response_content)

	def test_form(self):
		form_data = {'name': "the name", 'status' : "the status"}
		form = StatusForm(data= form_data)
		self.assertTrue(form.is_valid())
		request = self.client.post('/', data = form_data)
		self.assertEqual(request.status_code, 302)
		
		response = self.client.get('/')
		self.assertEqual(response.status_code, 200)

	def test_confirmation_get_method(self):
		response = self.client.get('/confirmation/')
		self.assertEqual(response.status_code, 302)

	def test_confirmation_post_method_form_is_valid(self):
		response = self.client.post('/confirmation/', {'name': "the name", 'status': "the status"})
		self.assertEqual(response.status_code, 200)
	
	def test_confirmation_post_method_form_is_not_valid(self):
		response = self.client.post('/confirmation/', {'name': "the name", 'status': ""})
		self.assertEqual(response.status_code, 302)

class FunctionalTest(LiveServerTestCase):

	def setUp(self):
		super().setUp()
		chrome_options = webdriver.ChromeOptions()
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('--disable-gpu')
		chrome_options.add_argument('--disable-dev-shm-usage')
		self.browser = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')


	def tearDown(self):
		self.browser.quit()
			
	def test_form_yes(self):
		time.sleep(2)
		self.browser.get(self.live_server_url)
		time.sleep(2)

		name = self.browser.find_element_by_id('id_name')
		status = self.browser.find_element_by_id('id_status')
		submit = self.browser.find_element_by_id('id_submit')

		name.send_keys('testName')
		status.send_keys('testStatus')
		time.sleep(2)
		submit.click()

		time.sleep(2)
		yes = self.browser.find_element_by_id('id_yes')
		yes.click()
		time.sleep(5)
		self.assertEqual(self.browser.current_url, self.live_server_url + "/")
		assert 'testName' in self.browser.page_source
		assert 'testStatus' in self.browser.page_source
	
	def test_form_no(self):
		time.sleep(2)
		self.browser.get(self.live_server_url)
		time.sleep(2)

		name = self.browser.find_element_by_id('id_name')
		status = self.browser.find_element_by_id('id_status')
		submit = self.browser.find_element_by_id('id_submit')

		name.send_keys('testName')
		status.send_keys('testStatus')
		time.sleep(2)
		submit.click()

		time.sleep(2)
		no = self.browser.find_element_by_id('id_no')
		no.click()
		time.sleep(5)
		self.assertEqual(self.browser.current_url, self.live_server_url + "/")
		self.assertNotIn('testName', self.browser.page_source)
		self.assertNotIn('testStatus', self.browser.page_source)

	def test_change_color(self):
		time.sleep(2)
		self.browser.get(self.live_server_url)
		time.sleep(2)

		name = self.browser.find_element_by_id('id_name')
		status = self.browser.find_element_by_id('id_status')
		submit = self.browser.find_element_by_id('id_submit')

		name.send_keys('testName')
		status.send_keys('testStatus')
		time.sleep(2)
		submit.click()

		time.sleep(2)
		yes = self.browser.find_element_by_id('id_yes')
		yes.click()
		time.sleep(2)
		self.assertEqual(self.browser.current_url, self.live_server_url + "/")

		status = self.browser.find_element_by_class_name('statusbox')
		defaultColour = status.value_of_css_property('color')
		self.assertEqual(defaultColour, 'rgba(255, 255, 255, 1)')
		time.sleep(2)

		changeColor = self.browser.find_element_by_id('id_changeColor')
		changeColor.click()
		time.sleep(2)
		status = self.browser.find_element_by_class_name('statusbox')
		changedColour = status.value_of_css_property('color')
		self.assertEqual(changedColour, 'rgba(0, 0, 0, 1)')







	

	
from django.db import models
from django.forms import ModelForm

# Create your models here.
class Status(models.Model):
	name = models.CharField('name', max_length=100)
	status = models.CharField('status', max_length=100)
	
	def __str__(self):
		return self.name